@echo off

if exist zebraTools (
	rd /q /s zebraTools 
)


pyinstaller.exe -F -w --icon=img/tools.ico ZebraTools.py


rd /q /s build

ren dist zebraTools

if not exist  zebraTools\img md zebraTools\img
xcopy img zebraTools\img

if not exist  zebraTools\splice md zebraTools\splice 
xcopy splice zebraTools\splice 

if not exist  zebraTools\bicDistribution md zebraTools\bicDistribution
xcopy bicDistribution zebraTools\bicDistribution

if not exist  zebraTools\calOffset md zebraTools\calOffset 
xcopy calOffset zebraTools\calOffset

if not exist  zebraTools\plot md zebraTools\plot
xcopy plot zebraTools\plot

if not exist  zebraTools\split md zebraTools\split
xcopy split zebraTools\split

if not exist  zebraTools\bin2txt md zebraTools\bin2txt
xcopy bin2txt zebraTools\bin2txt

exit
