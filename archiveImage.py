#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
import glob
import shutil

# import json
# import pprint
# import cPickle as pickle

###### Document Decription
'''  '''

###### Version and Date
prog_version = '1.0.0'
prog_date = '2015-04-03'

###### Usage
usage = '''

     Version %s  by Vincent-Li  %s

     Usage: %s <inputDir> <outputDir> <fov> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################


##########################################################################
############################  BEGIN Function  ############################
##########################################################################
def archiveImage(inputDir, outputDir, fov):
    fovList = fov.strip().split(',')
    for f in fovList:
        for cyclePath in glob.glob(os.path.join(inputDir, "C*")):
            cycle = os.path.basename(cyclePath)
            if cycle == 'Cleavge':
                continue
            outCyclePath = os.path.join(outputDir, cycle)
            if not os.path.exists(outCyclePath):
                os.makedirs(outCyclePath)
            fovFiles = glob.glob(os.path.join(cyclePath, "%s*.tif" % f))
            if len(fovFiles) > 0:
                for x in fovFiles:
                    fb = os.path.basename(x)
                    shutil.copyfile(x, os.path.join(outCyclePath, fb))
            else:
                print >>sys.stderr, "Can not find fov %s in cycle %s" % (f, cycle)

######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 3:
        ArgParser.print_help()
        print >>sys.stderr, "\nERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (inputDir, outputDir, fov) = args

    ############################# Main Body #############################
    archiveImage(inputDir, outputDir, fov)


#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################