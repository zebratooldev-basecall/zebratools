
#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')

#import subprocess
import time
from multiprocessing import Process
import threading
import subprocess

from Tkinter import *
import tkFileDialog
from plot import plot
from archiveImage import archiveImage
from scar import scar
import uploadData

###### Document Decription
''' Zebra Tools  '''

###### Change Log
'''
        2.1.0   2015.11.26  Update the uploadData module for new version
'''

###### Version and Date
prog_version = '2.1.0'

prog_date = '2015-04-24' 

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <TODO> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable
ENTRY_WIDTH = 55


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
def Tools():
    tk = Tk()
    tk.title('Tools')
    tk.geometry('600x90')
    icoImg = 'img/tools.ico'
    tk.iconbitmap(icoImg)

 
    def Plot():
        top = Toplevel(tk)
        top.title('Plot')
        top.iconbitmap(icoImg)
    
        plotText = StringVar()

        Label(top, text="Input").grid(row=0)
        plotAddr = Entry(top, textvariable=plotText, width=ENTRY_WIDTH)   
        plotAddr.grid(row=0, column=2)
        
        def findFile():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            plotText.set(filename)

        b = Button(top, text="search", command=findFile)
        b.grid(row=0,column=3)

        
        
        def runPlot():
            ### Get the file's name
            
            #time.sleep(3)
            filename = plotAddr.get()
            if not filename or not os.path.exists(filename):
                s.set("file not exists")
                return

            s.set('running')  
            top.update()

            plot(filename)

            s.set('finished') 

        
        runButton = Button(top, text="Run", command=runPlot)
        runButton.grid(row=1, column=2)

        s = StringVar()
        status = Message(top, width=100, textvariable=s)
        s.set('begin')
        status.grid(row=2, column=0)

    def CopyImage():
        top = Toplevel(tk)
        top.title('CopyImage')
        top.iconbitmap(icoImg)

        Label(top, text="inputDir").grid(row=0)
        copyImageAddrIn = Entry(top, text="input image's addr", width=ENTRY_WIDTH)   
        copyImageAddrIn.grid(row=0, column=2)

        Label(top, text="outputDir").grid(row=1)
        copyImageAddrOut = Entry(top, text="output image's addr", width=ENTRY_WIDTH)   
        copyImageAddrOut.grid(row=1, column=2)
   
        Label(top, text="fov").grid(row=2)
        fovList = Entry(top, text="fov", width=ENTRY_WIDTH)   
        fovList.grid(row=2, column=2)
        
        def runCopyImage():
            inputDir = copyImageAddrIn.get()
            outputDir = copyImageAddrOut.get()
            fov = fovList.get()

            if inputDir and outputDir and fov:
                status["text"] = "running"
                top.update()
                archiveImage(inputDir, outputDir, fov)
                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runCopyImage)
        runButton.grid(row=3, column=2)

        status = Label(top, text="begin")   
        status.grid(row=4, column=0)

    def Bin2txt():
        top = Toplevel(tk)
        top.title('Bin2txt')
        top.iconbitmap(icoImg)

        Label(top, text="inputDir").grid(row=0)
        inten_addr = Entry(top, text="Intensities dir", width=ENTRY_WIDTH)   
        inten_addr.grid(row=0, column=2)

        Label(top, text="fov").grid(row=1)
        fovList = Entry(top, text="Fov id", width=ENTRY_WIDTH)   
        fovList.grid(row=1, column=2)
        
        def runBin2txt():
            inputDir = inten_addr.get()
            fov = fovList.get()

            if inputDir and fov:
                status["text"] = "running"
                top.update()
                #bin2txt(inputDir, fov)
                #print (r'python bin2txt\bin2txt.py %s %s' % (inputDir, fov))
                os.popen(r'python bin2txt\bin2txt.py %s %s' % (inputDir, fov))
                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runBin2txt)
        runButton.grid(row=2, column=2)

        status = Label(top, text="begin")   
        status.grid(row=4, column=0)

    def Quencing():
        top = Toplevel(tk)
        top.title('Quencing')
        top.iconbitmap(icoImg)

        testFileText = StringVar()
        refFileText = StringVar()

        def findFile1():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            testFileText.set(filename)

        def findFile2():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            refFileText.set(filename)

        Label(top, text="Input test file").grid(row=0)
        testFileEntry = Entry(top, textvariable=testFileText, width=ENTRY_WIDTH)   
        testFileEntry.grid(row=0, column=2)
        b1 = Button(top, text="search", command=findFile1)
        b1.grid(row=0,column=3)

        Label(top, text="Input ref file").grid(row=1)
        refFileEntry = Entry(top, textvariable=refFileText, width=ENTRY_WIDTH)   
        refFileEntry.grid(row=1, column=2)
        b2 = Button(top, text="search", command=findFile2)
        b2.grid(row=1,column=3)

        Label(top, text="cycleStart").grid(row=2)
        cycleStartEntry = Entry(top, text="cycleStart", width=ENTRY_WIDTH)   
        cycleStartEntry.grid(row=2, column=2)

        Label(top, text="cycleInc").grid(row=3)
        cycleIncEntry = Entry(top, text="cycleInc", width=ENTRY_WIDTH)   
        cycleIncEntry.grid(row=3, column=2)

        Label(top, text="bases").grid(row=4)
        basesEntry = Entry(top, text="bases", width=ENTRY_WIDTH)   
        basesEntry.grid(row=4, column=2)

        Label(top, text="outDir").grid(row=5)
        outDirEntry = Entry(top, text="outDir", width=ENTRY_WIDTH)   
        outDirEntry.grid(row=5, column=2)

        
        
        def runQuencing():
            testFile = testFileEntry.get()
            refFile = refFileEntry.get()
            cycleStart = cycleStartEntry.get()
            cycleInc = cycleIncEntry.get()
            bases = basesEntry.get()
            outDir = outDirEntry.get()

            if testFile and refFile and cycleStart and cycleInc and bases and outDir:
                status["text"] = "running"
                top.update()
                scar(testFile, refFile, int(cycleStart), int(cycleInc), bases, outDir)
                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"

        runButton = Button(top, text="Run", command=runQuencing)
        runButton.grid(row=6, column=2)

        status = Label(top, text="begin")   
        status.grid(row=7, column=0)


    def SplitRate():
        top = Toplevel(tk)
        top.title('splitRate')
        top.iconbitmap(icoImg)

        barcodeText = StringVar()
        fastqText = StringVar()

        def findFile1():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            barcodeText.set(filename)

        def findFile2():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            fastqText.set(filename)

        Label(top, text="Input barcode").grid(row=0)
        barcodeFileEntry = Entry(top, textvariable=barcodeText, width=ENTRY_WIDTH)   
        barcodeFileEntry.grid(row=0, column=2)
        b1 = Button(top, text="search", command=findFile1)
        b1.grid(row=0,column=3)

        Label(top, text="Input fastq").grid(row=1)
        fastqFileEntry = Entry(top, textvariable=fastqText, width=ENTRY_WIDTH)   
        fastqFileEntry.grid(row=1, column=2)
        b2 = Button(top, text="search", command=findFile2)
        b2.grid(row=1,column=3)

        Label(top, text="OutDir").grid(row=2)
        outDirEntry = Entry(top, text="outDir", width=ENTRY_WIDTH)   
        outDirEntry.grid(row=2, column=2)

        Label(top, text="errNum(option)").grid(row=3)
        errNumEntry = Entry(top, text="errNum", width=ENTRY_WIDTH)   
        errNumEntry.grid(row=3, column=2)

        Label(top, text="barcodeStart").grid(row=4)
        barcodeStartEntry = Entry(top, text="barcodeStart", width=ENTRY_WIDTH)   
        barcodeStartEntry.grid(row=4, column=2)

        Label(top, text="barcodeLength").grid(row=5)
        barcodeLengthEntry = Entry(top, text="barcodeLength", width=ENTRY_WIDTH)   
        barcodeLengthEntry.grid(row=5, column=2)

        Label(top, text="read1Length(option)").grid(row=6)
        read1LengthEntry = Entry(top, text="read1Length", width=ENTRY_WIDTH)   
        read1LengthEntry.grid(row=6, column=2)

        Label(top, text="read2Length(option)").grid(row=7)
        read2LengthEntry = Entry(top, text="read2Length", width=ENTRY_WIDTH)   
        read2LengthEntry.grid(row=7, column=2)

        Label(top, text="select reverse").grid(row=8)
        select = StringVar()
        select.set('No')

        Radiobutton(top, variable=select, text='Yes', value='Yes').grid(row=8, column=2)
        Radiobutton(top, variable=select, text='No', value='No').grid(row=9, column=2)
        

        def runSplitRate():
            barcodeList = barcodeFileEntry.get()
            fastqFile = fastqFileEntry.get()
            outDir = outDirEntry.get()
            errNum = errNumEntry.get()
            barcodeStart = barcodeStartEntry.get()
            barcodeLength = barcodeLengthEntry.get()
            read1Length = read1LengthEntry.get()
            read2Length = read2LengthEntry.get()

            if barcodeList and fastqFile and outDir and barcodeStart and barcodeLength:
                cmd = '%s %s %s %s %s -D %s ' % (r'split\Cut_barcode.exe', fastqFile, barcodeList, int(barcodeStart), int(barcodeLength),
                                                        outDir)
                if errNum:
                    cmd += '-M %s ' % (int(errNum), )
                if read1Length:
                    cmd += '-1 %s ' % (read1Length, )
                if read2Length:
                    cmd += '-2 %s ' % (read2Length, )
                if select.get() == 'Yes':
                    cmd += '-R '

                status["text"] = "running"
                top.update()
                #print cmd
                os.popen(cmd)
                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"

        runButton = Button(top, text="Run", command=runSplitRate)
        runButton.grid(row=10, column=2)

        status = Label(top, text="begin")   
        status.grid(row=11, column=0)
        
        Des = '''
        cmd Fq.gz barcodeFile 35 10
              SE: 35+10
        cmd Fq.gz barcodeFile 1 10
            SE: 10+35
        cmd Fq.gz barcodeFile 100 10  -1 50 -2 50
            PE: 50+50+10
        cmd Fq.gz barcodeFile 100 8  -1 50 -2 50
            PE: 50+50+8
        cmd Fq.gz barcodeFile 100 8  -1 50 -2 50 -R
            PE: 50+50+8 reverse barcode sequence
            '''
        Label(top, text=Des).grid(row=12, columnspan=3)

    def Splice():
        top = Toplevel(tk)
        top.title('Splice')
        top.iconbitmap(icoImg)

        Label(top, text="ImageDir").grid(row=0)
        imageDirEntry = Entry(top, textvariable='imageDir', width=ENTRY_WIDTH)   
        imageDirEntry.grid(row=0, column=2)
        
        Label(top, text="OutDir").grid(row=1)
        OutDirEntry = Entry(top, textvariable='OutDir', width=ENTRY_WIDTH)   
        OutDirEntry.grid(row=1, column=2)

        Label(top, text="Base").grid(row=2)
        BaseEntry = Entry(top, textvariable='Base', width=ENTRY_WIDTH)   
        BaseEntry.grid(row=2, column=2)

        Label(top, text="RC").grid(row=3)
        RCValue = StringVar()
        RCEntry = Entry(top, textvariable=RCValue, width=ENTRY_WIDTH)   
        RCEntry.grid(row=3, column=2)
        RCValue.set('default')

        Label(top, text="scale").grid(row=4)
        v = StringVar()
        scaleEntry = Entry(top, textvariable=v, width=ENTRY_WIDTH)   
        scaleEntry.grid(row=4, column=2)
        v.set('0.125')

        Label(top, text="select format").grid(row=5)
        select = StringVar()
        select.set('png')

        Radiobutton(top, variable=select, text='png', value='png').grid(row=5, column=2)
        Radiobutton(top, variable=select, text='tif', value='tif').grid(row=6, column=2)

        def runSplice():
            imageDir = imageDirEntry.get()
            outDir = OutDirEntry.get()
            base = BaseEntry.get()
            RC = RCEntry.get()
            scale = scaleEntry.get()
            program = r'splice\splice_png.exe' if select.get() == 'png' else r'splice\splice_tif.exe'
           

            if imageDir and outDir and base:
                status["text"] = "running"
                top.update()
                if RC == 'default':
                    os.popen('%s %s %s %s' % (program, imageDir, outDir, base))
                else:
                    os.popen('%s %s %s %s %s %s' % (program, imageDir, outDir, base, RC, scale))

                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runSplice)
        runButton.grid(row=7, column=2)

        status = Label(top, text="begin")   
        status.grid(row=8, column=0)

    def CalOffset():
        top = Toplevel(tk)
        top.title('CalOffset')
        top.iconbitmap(icoImg)

        Label(top, text="ImageDir").grid(row=0)
        imageDirEntry = Entry(top, textvariable='imageDir', width=ENTRY_WIDTH)   
        imageDirEntry.grid(row=0, column=2)
        
        Label(top, text="OutDir").grid(row=1)
        OutDirEntry = Entry(top, textvariable='OutDir', width=ENTRY_WIDTH)   
        OutDirEntry.grid(row=1, column=2)

        Label(top, text="CycleNum").grid(row=2)
        CycleNumEntry = Entry(top, textvariable='CycleNum', width=ENTRY_WIDTH)   
        CycleNumEntry.grid(row=2, column=2)

        Label(top, text="ColNum").grid(row=3)
        ColNumEntry = Entry(top, textvariable='ColNum', width=ENTRY_WIDTH)   
        ColNumEntry.grid(row=3, column=2)
        
        Label(top, text="RowNum").grid(row=4)
        RowNumEntry = Entry(top, textvariable='RowNum', width=ENTRY_WIDTH)   
        RowNumEntry.grid(row=4, column=2)

        
        

        def runCalOffset():
            imageDir = imageDirEntry.get()
            outDir = OutDirEntry.get()
            cycleNum = int(CycleNumEntry.get())
            rowNum = int(RowNumEntry.get())
            colNum = int(ColNumEntry.get())
            program = r'calOffset\ia310.exe'
           

            if imageDir and outDir and cycleNum and rowNum and colNum:
                status["text"] = "running"
                top.update()
                
                os.popen('%s %s %s %s %s %s' % (program, imageDir, outDir, cycleNum, colNum, rowNum))

                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runCalOffset)
        runButton.grid(row=5, column=2)

        status = Label(top, text="begin")   
        status.grid(row=6, column=0)

        
    def bicDis():
        top = Toplevel(tk)
        top.title('BicDis')
        top.iconbitmap(icoImg)

        Label(top, text="ImageDir").grid(row=0)
        imageDirEntry = Entry(top, textvariable='imageDir', width=ENTRY_WIDTH)   
        imageDirEntry.grid(row=0, column=2)
        
        Label(top, text="OutDir").grid(row=1)
        OutDirEntry = Entry(top, textvariable='OutDir', width=ENTRY_WIDTH)   
        OutDirEntry.grid(row=1, column=2)

        Label(top, text="ColNum").grid(row=2)
        ColNumEntry = Entry(top, textvariable='ColNum', width=ENTRY_WIDTH)   
        ColNumEntry.grid(row=2, column=2)
        
        Label(top, text="RowNum").grid(row=3)
        RowNumEntry = Entry(top, textvariable='RowNum', width=ENTRY_WIDTH)   
        RowNumEntry.grid(row=3, column=2)

        
        

        def runbicDis():
            imageDir = imageDirEntry.get()
            outDir = OutDirEntry.get()
            cycleNum = int(1)
            rowNum = int(RowNumEntry.get())
            colNum = int(ColNumEntry.get())
            program = r'bicDistribution\basecall_bicAnalysis.exe'
           

            if imageDir and outDir and cycleNum and rowNum and colNum:
                status["text"] = "running"
                top.update()
                
                os.popen('%s %s %s %s %s %s' % (program, imageDir, outDir, cycleNum, colNum, rowNum))

                status["text"] = "finished"
                if os.path.exists('bic.txt'):
                    os.remove('bic.txt')
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runbicDis)
        runButton.grid(row=5, column=2)

        status = Label(top, text="begin")   
        status.grid(row=6, column=0)
   
    def UploadData():
        top = Toplevel(tk)
        top.title('UploadData')
        top.iconbitmap(icoImg)

        FileNameText = StringVar()
        Label(top, text="FileName").grid(row=0)
        FileNameEntry = Entry(top, textvariable=FileNameText, width=ENTRY_WIDTH)   
        FileNameEntry.grid(row=0, column=2)
        
        def findFile():
            filename = tkFileDialog.askopenfilename(initialdir = 'E:/')
            FileNameText.set(filename)

        b = Button(top, text="search", command=findFile)
        b.grid(row=0,column=3)

        Label(top, text="select PE or SE").grid(row=1)
        selectType = StringVar()
        selectType.set('SE')

        Radiobutton(top, variable=selectType, text='SE', value='SE').grid(row=1, column=2)
        Radiobutton(top, variable=selectType, text='PE', value='PE').grid(row=2, column=2)
        
        Label(top, text="select Reference").grid(row=3)
        selectRef = StringVar()
        selectRef.set('Ecoli.fa')

        Radiobutton(top, variable=selectRef, text='Ecoli.fa', value='Ecoli.fa').grid(row=3, column=2)
        Radiobutton(top, variable=selectRef, text='Human.fa', value='Human.fa').grid(row=4, column=2)
        

        def runUploadData():
            FileName = FileNameEntry.get()
            seqType = selectType.get()
            reference = selectRef.get()
            # print FileName, seqType, reference
            if FileName and seqType and reference:
                status["text"] = "running"
                top.update()
                
                ud = uploadData.UploadData(FileName, r'C:\cwRsync\uploadConfig.ini',  seqType, reference, True, True)
                ud.copyData()

                status["text"] = "finished"
            else:
                status["text"] = "wrong parameters"


        runButton = Button(top, text="Run", command=runUploadData)
        runButton.grid(row=5, column=2)

        status = Label(top, text="begin")   
        status.grid(row=6, column=0)
    ### 1. Main interface, four button.
    menu = Menu(tk)
    

    menu.add_command(label = 'Plot', command=Plot)
    menu.add_command(label = 'CopyImage', command=CopyImage)
    menu.add_command(label = 'Bin2txt', command=Bin2txt)
    menu.add_command(label = 'Quencing', command=Quencing)
    menu.add_command(label = 'SplitRate', command=SplitRate)
    menu.add_command(label = 'Splice', command=Splice)
    menu.add_command(label = 'CalOffset', command=CalOffset)
    menu.add_command(label = 'BicDis', command=bicDis)
    menu.add_command(label = 'UploadData', command=UploadData)

    tk["menu"] = menu

    

    tk.mainloop()
        



##########################################################################
############################  BEGIN Function  ############################
##########################################################################




######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 0:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    ############################# Main Body #############################
    Tools()

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


