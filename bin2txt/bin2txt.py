import sys, os
import glob

def bin2txt(inten_addr, fov):
    cur_path = os.path.dirname(os.path.realpath(__file__))
    for ints in ['finInts', 'midInts', 'rawInts']:
        addr = os.path.join(inten_addr, ints)
        if not os.path.exists(addr): continue
        
        for cycle_addr in glob.glob(os.path.join(addr, "S*")):
            bin_file = os.path.join(cycle_addr, '%s.bin' % fov)
            if not os.path.exists(bin_file):
                raise Exception, 'Do not exists %s' % bin_file
            #os.popen(r'%s\bin2txt.exe %s' % (cur_path, bin_file))
            txt_file = os.path.join(cycle_addr, '%s.txt' % fov)
            if not os.path.exists(txt_file):
                raise Exception, "Bin2txt Error in %s" % cycle_addr
        array = []
        file_list = []
        for cycle_addr in glob.glob(os.path.join(addr, "S*")):
            txt_file = os.path.join(cycle_addr, '%s.txt' % fov)
            if os.path.exists(txt_file):
                file_list.append(txt_file)

            #with open(txt_file, 'r') as fh_in:
            #    if not array:
            #        for idx, line in enumerate(fh_in):
            #            array.append(line.strip().split())
            #    else:
            #        for idx, line in enumerate(fh_in):
            #            array[idx].extend(line.strip().split())


        #with open(os.path.join(inten_addr, '%s.txt' % ints), 'w') as fh_out:
        #    for line in array:
        #        fh_out.write('\t'.join(map(str, line)) + '\n')
        with open(os.path.join(inten_addr, 'file_list1.txt'), 'w') as fh_out:
            fh_out.write('\n'.join(file_list))
        break
            
def bin2txt_v2(inten_addr, fov):
    cur_path = os.path.dirname(os.path.realpath(__file__))
    for ints in ['finInts', 'midInts', 'rawInts']:
        addr = os.path.join(inten_addr, ints)
        if not os.path.exists(addr): continue
        
        for cycle_addr in glob.glob(os.path.join(addr, "S*")):
            bin_file = os.path.join(cycle_addr, '%s.bin' % fov)
            if not os.path.exists(bin_file):
                raise Exception, 'Do not exists %s' % bin_file
            os.popen(r'%s\bin2txt.exe %s' % (cur_path, bin_file))
            txt_file = os.path.join(cycle_addr, '%s.txt' % fov)
            if not os.path.exists(txt_file):
                raise Exception, "Bin2txt Error in %s" % cycle_addr

        ### Search all txt files
        array = []
        file_list = []
        for cycle_addr in glob.glob(os.path.join(addr, "S*")):
            txt_file = os.path.join(cycle_addr, '%s.txt' % fov)
            if os.path.exists(txt_file):
                file_list.append(txt_file)

        ### Get all file handle
        fh_list = []
        for f in file_list:
            fh = open(f, 'r')
            fh_list.append(fh)

        ### Read line_inc line in memory and dump the file every cycle
        line_inc = 50000 #100000
        fh_out = open(os.path.join(inten_addr, '%s.txt' % ints), 'w')
        while True:
            array = []
            for fh in fh_list:
                if not array:
                    for idx, line in enumerate(fh):
                        array.append(line.strip().split())
                        if idx == line_inc-1: break
                else:
                    for idx, line in enumerate(fh):
                        array[idx].extend(line.strip().split())
                        if idx == line_inc-1: break
            if not array:
                break
            elif len(array) != line_inc:
                for line in array:
                    fh_out.write('\t'.join(map(str, line)) + '\n')
                break
            else:
                for line in array:
                    fh_out.write('\t'.join(map(str, line)) + '\n')
            array = []

        ### Release the file handle
        for fh in fh_list:
            fh.close()
        fh_out.close()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "python %s <IntensitiesDir> <FovId>" % sys.argv[0]
        sys.exit()
    inten_addr = sys.argv[1]
    fov = sys.argv[2]

    # bin2txt(inten_addr, fov)
    bin2txt_v2(inten_addr, fov)
