# -*- mode: python -*-
a = Analysis(['tools.py'],
             pathex=['E:\\09.git\\script\\zebraTools'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='tools.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , icon='img\\tools.ico')
