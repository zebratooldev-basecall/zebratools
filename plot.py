
#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')

import matplotlib, sys, os
matplotlib.use('Agg')

from matplotlib.colors import LogNorm
from pylab import *
import fileinput
import numpy as np
import matplotlib.pyplot as plt
###### Document Decription
''' Plot crosstalk '''

###### Version and Date
prog_version = '1.0.0'

prog_date = '2015-04-30' 

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <file> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable

#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class plot(object):

    def plot4ChanCrossTalk(self, baseInfoList,outputFile):
        bases = "ACGT"
    
        fig, axarr = matplotlib.pyplot.subplots(nrows=2, ncols=3)#,sharex='col',sharey='row')   
        fig.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.88, wspace=0.18, hspace=0.25)
        num = 0

        for i in xrange(4):
            for j in xrange(i+1,4):
                if(num>=3):
                    m = 1;n=num-3
                else:
                    m=0;n=num

                maxi = max([max(baseInfoList[g]) for g in xrange(len(baseInfoList))])
                mini = min([min(baseInfoList[g]) for g in xrange(len(baseInfoList))])

                im = axarr[m,n].hist2d(baseInfoList[i], baseInfoList[j], bins=139,norm=LogNorm())
                num+=1
                axarr[m,n].set_title(bases[i] + bases[j])
                axarr[m,n].grid(True)
                axarr[m,n].axis([mini, maxi, mini, maxi])
                axarr[m,n].xaxis.get_label().set_size('xx-large')
                axarr[m,n].yaxis.get_label().set_size('xx-large')
                for label in axarr[m,n].xaxis.get_ticklabels():
                    label.set_fontsize(7)
                    label.set_rotation('45')
                for label in axarr[m,n].yaxis.get_ticklabels():
                    label.set_fontsize(7) 
        
        cbar_ax = fig.add_axes([0.0001, 0.0001, 0.0001, 0.0001])
        H, xedges, yedges = np.histogram2d(baseInfoList[0], baseInfoList[1],bins=139)
        im =  imshow(H)
  
        subplot_tool()
        xlabel('number')
        fig.suptitle('Pairwise Intensity Crosstalk Plot',multialignment='left',position=(0.45,0.95), fontsize=10,style='italic',weight=700)
        plt.savefig(outputFile,dpi=250)       
        plt.close('all')
        
    def extractData(self, inten):
        minThre = np.percentile(inten, 1, axis=0)
        maxThre = np.percentile(inten, 99, axis=0)
        lst = []
     
        for i in inten:
            filtered = True
            for j in xrange(4):
                if i[j] > maxThre[j] or i[j] < minThre[j]:
                    filtered = False
                    break 
            if filtered:
                lst.append(i)

        return lst

    def prepare(self, lst):
        baseA = []
        baseC = []
        baseG = []
        baseT = []
        for i in lst:
            baseA.append(float(i[0]))
            baseC.append(float(i[1]))
            baseG.append(float(i[2]))
            baseT.append(float(i[3]))
        nA = np.array(baseA)
        nC = np.array(baseC)
        nG = np.array(baseG)
        nT = np.array(baseT)

        totalList = []
        totalList.append(nA)
        totalList.append(nC)
        totalList.append(nG)
        totalList.append(nT)

        return totalList

    def _sampling(self, fileIn, fileOut):
        sampleThreshold = 50000

        fhOut = open(fileOut, 'w')
        
        count = -1
        for count, line in enumerate(open(fileIn, 'rU')):
            pass
        count += 1

        rowNum = count
        

        if rowNum < sampleThreshold:
            fhIn = open(fileIn, 'r')
            fhOut.write(fhIn.read())
            fhIn.close()

        else:
            threshold = rowNum / sampleThreshold    
            for line in fileinput.input(fileIn):
                num = fileinput.lineno()
                if num % threshold == 0:
                    fhOut.write(line)

            fileinput.close()

        fhOut.close()

    def drawAllCycleCrosstalk(self):
        mat = np.loadtxt(self.newFile)
        
        addr = os.path.join(self.addr, '%s.Plot' % self.flag)
        if os.path.exists(addr):
            if not os.path.isdir(addr):
                os.remove(addr)
                os.mkdir(addr)
            else:
                pass
        else:
            os.mkdir(addr)

        shape = mat.shape
        cycleMax = shape[1] / 4
        plt.figure.max_open_warning = 0
        for cycle in xrange(cycleMax):
            inten = mat[:,cycle*4:((cycle+1)*4)]
            lst = self.extractData(inten)
            #lst = inten

            totalList = self.prepare(lst)
            # Add name's choice
            if self.filename.endswith('.bin'):
                name = "cycle.png"
            else:
                name = "cycle%s.png" % (cycle+1,)
            self.plot4ChanCrossTalk(totalList,os.path.join(addr, name))


    def __init__(self, filename):
        if not os.path.exists(filename):
            print "%s is not exists!!!" % filename
            return

        self.filename = filename
        self.addr = os.path.dirname(filename)
        self.newFile = os.path.join(self.addr, 'sampling.txt')
        
        #########add bin format##########
        ######if (filename)######change in main
        # print "bin format"
        if(os.path.splitext(filename)[1] == ".bin" ):
            os.popen("plot\sampleBin.exe %s %s" % (filename , self.newFile))
            self.flag = os.path.basename(filename)
        else:  ############txt format##########    
            self.flag = os.path.basename(filename)
            self._sampling(self.filename, self.newFile)

        ### run plot
        try:
            self.drawAllCycleCrosstalk()
        except Exception as e:
            raise e
            pass
        self._removeTmpFile()

    def _removeTmpFile(self):
        if os.path.exists(self.newFile):
            os.remove(self.newFile)

    # def process(self):
    #     self._sampling(self.filename, self.newFile)
    #     try:
    #         self.drawAllCycleCrosstalk()
    #     except Exception:
    #         self._removeTmpFile()
            
    #     self._removeTmpFile()

##########################################################################
############################  BEGIN Function  ############################
##########################################################################




######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 1:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (file, ) = args

    ############################# Main Body #############################
    p = plot(file)

    try:
        p.drawAllCycleCrosstalk()
    except Exception:
        p._removeTmpFile()
        
    p._removeTmpFile()
    
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


