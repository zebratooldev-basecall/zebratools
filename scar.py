
#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')

import numpy as np

###### Document Decription
''' TODO '''

###### Version and Date
prog_version = '1.0.0'

prog_date = '2015-05-06' 

###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <testFile> <refFile> <cycleStart> <cycleInc> <bases> <outDir> >STDOUT
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable

#######################################################################
############################  BEGIN Class  ############################
#######################################################################
def scar(testFile, refFile, cycleStart, cycleInc, bases, outDir):
    lst = []
    lineno = 1
    base = []
    quality = []
    intensity = []

    
    cycle = cycleInc
    cycleEnd = cycleStart + cycleInc - 1
    
    if not os.path.exists(testFile) or not os.path.exists(refFile):
        return

    fhIn = open(testFile, 'r')
    for line in fhIn:
        data = line.strip().split()
        intensity.append(map(float, (data[(cycleEnd-1)*4:cycleEnd*4])))
        
    fhIn.close()

    ### 1. Read five cycle's intensity and store Fived cycle's inten and bases and quality
    fhIn = open(refFile, 'r')
    for line in fhIn:
        data = line.strip().split()[(cycleStart-1)*4 : cycleEnd*4]
        b= ''
        q= []

        
        for c in xrange(cycle):
            d = map(float, data[c*4:(c+1)*4])
            maxValue = max(d)
            maxPos = d.index(maxValue)
            try:
                secondValue = max([v for v in d if v != maxValue])
            except ValueError:
                secondValue = maxValue
                p = 0.5
            else:
                try:
                    p = float(maxValue) / (maxValue+secondValue)
                except ZeroDivisionError:
                    p = 1
            b += bases[maxPos]
            q.append(p)

    #    intensity.append(map(int, data[(cycle-1)*4:cycle*4]))
        base.append(b)
        quality.append(q)

        lineno += 1

    ### 2. Filtered the worst 10% clusters.
    quality = np.array(quality)

    minValues = np.min(quality, axis=1)
    threshold = np.percentile(minValues, 10)

    dict = {}

    ### 3. Construct a dict to store the 1024 groups intensity and get average.
    for i in xrange(len(minValues)):
        if minValues[i] > threshold:
            dict.setdefault(base[i], []).append(intensity[i])


    fhOut = open(os.path.join(outDir, 'zebra-cycle%d-%d-%s-%d-scardNTP.csv' % (cycleStart, cycleEnd, os.path.basename(testFile), cycleInc)), 'w')

    #fhOut.write(',' + ','.join(bases) + '\n')
    fhOut.write(','*5 + ','.join(bases) + '\n')
    for k in sorted(dict.keys()):
        mat = np.array(dict[k])
        ave = np.average(mat, axis=0)
        #ave = ave.tolist()

        # The key in one column
        #fhOut.write(k+','+','.join(map(str,map(int, ave))) + '\n')
        # The key in five column
        fhOut.write(','.join(k)+','+','.join(map(str,map(float, ave))) + '\n')

    fhOut.close()



##########################################################################
############################  BEGIN Function  ############################
##########################################################################




######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 6:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (testFile, refFile, cycleStart, cycleInc, bases, outDir) = args

    ############################# Main Body #############################
    cycleStart = int(cycleStart)
    cycleInc = int(cycleInc)

    scar(testFile, refFile, cycleStart, cycleInc, bases, outDir)
   

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


