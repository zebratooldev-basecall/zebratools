
#!/usr/bin/env python
#coding: utf8

###### Import Modules
import sys, os
reload(sys)
sys.setdefaultencoding('utf8')

import subprocess
import shutil
import datetime
import glob

from copyData import CopyData
###### Document Decription
''' Upload data from sequence machine to cluster when offline model '''

###### Version and Date
prog_version = '0.1.0'

prog_date = '2015-11-26' 

changelog = '''
            '''
###### Usage
usage = '''

     Version %s  by zhaofuxiang  %s

     Usage: %s <dataInfoFile> <configFile> <seqType> <reference>
''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))

######## Global Variable
# rsync = os.path.join(currentDir, 'cwRsync', 'rsync.exe')
# ssh = os.path.join(currentDir, 'cwRsync', 'ssh.exe')

#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class UploadData(CopyData):

    def __init__(self, dataInfoFile, configFile, seqType, reference, upload=True, searchDir=False):
        CopyData.__init__(self, dataInfoFile, configFile, None, upload, searchDir)

        # Fake data that I can use the method of CopyData Class directly
        self.seqType = seqType
        self.reference = reference
        if self.seqType == 'SE':
            self.laneInfoDict['read1_length'] = 25
            self.laneInfoDict['read2_length'] = 0
        else:
            self.laneInfoDict['read1_length'] = 25
            self.laneInfoDict['read2_length'] = 25
        self.laneInfoDict['output'] = dataInfoFile
        self.laneInfoDict['reference'] = self.reference

    def copyData(self):
        # 1. Parse parameters from files
        if self.readConfig() and self.getDataInfo():
            if not self.parseParameter():
                return
        else:
            return
        # 2. Process extra data to uploaded.
        # In this case, do not upload images and performance
        self.uploadPerformance = False

        # 3. Start upload data to cluster
        self.prepareData(self.copyedDir)
        try:
            if self.select == 'remote':
                self.uploadRemoteData()
            elif self.select == 'local':
                self.uploadLocalData()
            else:
                raise Exception, "Select Error, %s." % self.select
        except Exception as e:
            print 'Load failed.'
            raise e
            self.transferStatus = 'Failed'

        # 4. Add lane's info to seqDataInfo.txt
        self.uploadInfo()

        # 5. Remove images after upload
        # In this case, do not have image dir

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################


#################################
##
##   Main function of program.
##
#################################
def Main():
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage = usage, version = prog_version)
    #ArgParser.add_argument("-i", "--imagePath", action="store", dest="imagePath", default=None, help="Input images path to upload. [%(default)s]")
    ArgParser.add_argument("-n", "--notUpload", action="store_false", dest="upload", default=True, help="Is upload data to cluster. [%(default)s]")
    ArgParser.add_argument("-s", "--searchDir", action="store_true", dest="searchDir", default=False, help="If data was copied to other dir, It will search dir for search fq and etc. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 4:
        ArgParser.print_help()
        print >>sys.stderr, "ERROR: The parameters number is not correct!"
        sys.exit(1)
    else:
        (dataInfoFile, configFile, seqType, reference) = args

    ############################# Main Body #############################
    startTime = datetime.datetime.now()
    print 'Upload Start Time: %s\n' % startTime.strftime("%Y-%m-%d %H:%M:%S")

    ud = UploadData(dataInfoFile, configFile, seqType, reference ,para.upload, para.searchDir)
    ud.copyData()

    endTime = datetime.datetime.now()
    print 'Upload End Time: %s\n' % endTime.strftime("%Y-%m-%d %H:%M:%S")
    print 'Upload result spend time %s\n' % (endTime - startTime)
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    Main()


